package ru.rudakova.project.store.client.web.model;

public record OrderItemDto(ChristmasTree christmasTree, int amount) {
}
