package ru.rudakova.project.store.client.web;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import ru.rudakova.project.store.client.services.OrderMapper;
import ru.rudakova.project.store.client.services.ServerStoreService;
import ru.rudakova.project.store.client.web.model.OrderItemDto;

import java.util.List;

@Path("/api")
public class OrderResource {

    @Inject
    @RestClient
    ServerStoreService serverStoreService;

    @Inject
    OrderMapper orderMapper;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTrees() {
        return serverStoreService.getTrees();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createOrder(List<OrderItemDto> orderItems) {
        serverStoreService.createOrder(orderMapper.convertToServerOrderModel(orderItems));
        return Response.ok().build();
    }
}