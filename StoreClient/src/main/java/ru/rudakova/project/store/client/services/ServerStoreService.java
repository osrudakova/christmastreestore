package ru.rudakova.project.store.client.services;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import ru.rudakova.project.store.client.web.model.OrderDto;

@Path("/api")
@RegisterRestClient(configKey = "server-store-services-api")
public interface ServerStoreService {

    @GET
    @Path("/trees")
    Response getTrees();

    @POST
    @Path("/order")
    Response createOrder(OrderDto orderDto);
}