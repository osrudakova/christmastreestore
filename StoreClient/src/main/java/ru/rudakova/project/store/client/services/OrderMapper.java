package ru.rudakova.project.store.client.services;

import jakarta.enterprise.context.ApplicationScoped;
import ru.rudakova.project.store.client.web.model.OrderDto;
import ru.rudakova.project.store.client.web.model.OrderItemDto;

import java.util.List;

@ApplicationScoped
public class OrderMapper {
    public OrderDto convertToServerOrderModel(List<OrderItemDto> orderItems) {
        return new OrderDto(orderItems, "");
    }
}
