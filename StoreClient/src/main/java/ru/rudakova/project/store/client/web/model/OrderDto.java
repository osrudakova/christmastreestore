package ru.rudakova.project.store.client.web.model;

import java.util.List;

public record OrderDto(List<OrderItemDto> orderItems, String contactPhone) {
}
