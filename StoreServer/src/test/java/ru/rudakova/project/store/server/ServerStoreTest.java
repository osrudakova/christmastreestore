package ru.rudakova.project.store.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.rudakova.project.store.server.model.ChristmasTree;
import ru.rudakova.project.store.server.model.Order;
import ru.rudakova.project.store.server.model.OrderItem;

import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ServerStoreTest extends AbstractSpringApplicationTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testOrderCreation() throws Exception {
        ChristmasTree christmasTree = createChristmasTree();

        mockMvc.perform(post("/api/trees")
                        .content(objectMapper.writeValueAsString(List.of(christmasTree)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        MvcResult result = mockMvc.perform(get("/api/trees"))
                .andExpect(status().isOk())
                .andReturn();

        ChristmasTree[] content = objectMapper.readValue(result.getResponse().getContentAsString(), ChristmasTree[].class);
        assertThat("Christmas tree should have given name", content[0].getName().equals(christmasTree.getName()));

        Order order = new Order();
        OrderItem orderItem = new OrderItem();
        orderItem.setChristmasTree(content[0]);
        orderItem.setAmount(2);
        order.setOrderItems(List.of(orderItem));
        order.setContactPhone("+79991232323");

        result = mockMvc.perform(post("/api/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(order)))
                .andExpect(status().isCreated())
                .andReturn();

        Order orderResult = objectMapper.readValue(result.getResponse().getContentAsString(), Order.class);
        assertThat("Order item should contain christmas tree", orderResult.getOrderItems().get(0).getChristmasTree().getId() == content[0].getId());
        assertThat("Price should be " + christmasTree.getPrice() * 2, christmasTree.getPrice() * 2 == orderResult.getSumPrice());
    }

    private ChristmasTree createChristmasTree() {
        Random random = new Random();

        ChristmasTree christmasTree = new ChristmasTree();
        christmasTree.setColor("green");
        christmasTree.setDiameter(random.nextInt(1000));
        christmasTree.setHeight(random.nextInt(1000));
        christmasTree.setName("Tree" + random.nextInt(1000));
        christmasTree.setNatural(true);
        christmasTree.setPrice(random.nextDouble(1000));
        christmasTree.setType("tree");
        return christmasTree;
    }
}
