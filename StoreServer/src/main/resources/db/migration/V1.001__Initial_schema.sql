create table if not exists christmas_tree
(
    id bigint not null
    constraint christmas_tree_pkey
    primary key,
    color varchar(255) not null,
    diameter integer,
    height integer not null,
    is_natural boolean not null,
    name varchar(255) not null,
    type varchar(255)
    );

create sequence christmas_tree_seq
    increment by 50;

alter table christmas_tree owner to postgres;
alter sequence christmas_tree_seq owner to postgres;