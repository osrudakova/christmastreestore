create sequence order_item_seq
    increment by 50;

alter sequence order_item_seq owner to postgres;

create sequence orders_seq
    increment by 50;

alter sequence orders_seq owner to postgres;

create table if not exists orders
(
    id bigint not null
        constraint orders_pkey
            primary key,
    contact_phone varchar(255) not null,
    sum_price double precision
);

alter table orders owner to postgres;

create table if not exists order_item
(
    id bigint not null
        constraint order_item_pkey
            primary key,
    amount integer not null,
    christmas_tree_id bigint
        constraint fk583hcu1nmc9e7ihbsxvhxe79q
            references christmas_tree,
    order_id bigint
        constraint fkt4dc2r9nbvbujrljv3e23iibt
            references orders
);

alter table order_item owner to postgres;

create table if not exists order_order_items
(
    order_id bigint not null,
    order_items_id bigint not null
        constraint uk_o1v5uvk9qjm4yl55y330ngkyc
            unique
        constraint fkays1fnrp2ptukxk0g1lemyi3f
            references order_item
);

alter table order_order_items owner to postgres;



alter table christmas_tree add column price double precision not null default 0;