package ru.rudakova.project.store.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rudakova.project.store.server.model.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
