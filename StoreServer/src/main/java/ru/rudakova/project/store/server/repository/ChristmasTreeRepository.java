package ru.rudakova.project.store.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rudakova.project.store.server.model.ChristmasTree;

import java.util.List;

public interface ChristmasTreeRepository extends JpaRepository<ChristmasTree, Long> {

}
