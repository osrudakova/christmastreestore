package ru.rudakova.project.store.server.service;

import ru.rudakova.project.store.server.model.OrderItem;

import java.util.List;

public interface OrderItemService {
    List<OrderItem> saveAll(List<OrderItem> orderItems);
}
