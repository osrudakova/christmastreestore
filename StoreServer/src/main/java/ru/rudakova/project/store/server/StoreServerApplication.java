package ru.rudakova.project.store.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("ru.rudakova.project.store.server.repository")
@EntityScan("ru.rudakova.project.store.server.model")
@SpringBootApplication
public class StoreServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(StoreServerApplication.class, args);
    }
}