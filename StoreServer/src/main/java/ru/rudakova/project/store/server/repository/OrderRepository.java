package ru.rudakova.project.store.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rudakova.project.store.server.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
