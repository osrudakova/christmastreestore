package ru.rudakova.project.store.server.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rudakova.project.store.server.model.Order;
import ru.rudakova.project.store.server.model.OrderItem;
import ru.rudakova.project.store.server.repository.OrderRepository;
import ru.rudakova.project.store.server.service.ChristmasTreeService;
import ru.rudakova.project.store.server.service.OrderItemService;
import ru.rudakova.project.store.server.service.OrderService;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ChristmasTreeService christmasTreeService;

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public Order create(Order order) {
        double resultSum = order.getOrderItems()
                .stream()
                .peek(orderItem -> orderItem.setOrder(order))
                .peek(orderItem -> orderItem.setChristmasTree(christmasTreeService.findById(orderItem.getChristmasTree().getId())))
                .map(orderItem -> orderItem.getChristmasTree().getPrice() * orderItem.getAmount())
                .reduce(0d, Double::sum);
        order.setSumPrice(resultSum);
        return orderRepository.save(order);
    }
}
