package ru.rudakova.project.store.server.service;

import ru.rudakova.project.store.server.model.ChristmasTree;

import java.util.List;

public interface ChristmasTreeService {
    List<ChristmasTree> findAll();
    ChristmasTree findById(long id);
    List<ChristmasTree> bulkCreate(List<ChristmasTree> christmasTrees);
}
