package ru.rudakova.project.store.server.service;

import ru.rudakova.project.store.server.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> findAll();
    Order create(Order order);
}
