package ru.rudakova.project.store.server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.rudakova.project.store.server.model.ChristmasTree;
import ru.rudakova.project.store.server.service.ChristmasTreeService;

import java.util.List;

@RestController
@RequestMapping("/api/trees")
public class ChristmasTreeController {

    @Autowired
    private ChristmasTreeService christmasTreeService;

    @GetMapping
    public List<ChristmasTree> findAll() {
        return christmasTreeService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<ChristmasTree> bulkCreateTree(@RequestBody List<ChristmasTree> christmasTrees) {
        return christmasTreeService.bulkCreate(christmasTrees);
    }
}
