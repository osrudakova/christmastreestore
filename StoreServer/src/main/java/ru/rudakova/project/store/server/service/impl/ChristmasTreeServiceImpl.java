package ru.rudakova.project.store.server.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rudakova.project.store.server.model.ChristmasTree;
import ru.rudakova.project.store.server.repository.ChristmasTreeRepository;
import ru.rudakova.project.store.server.service.ChristmasTreeService;

import java.util.List;

@Service
public class ChristmasTreeServiceImpl implements ChristmasTreeService {

    @Autowired
    private ChristmasTreeRepository christmasTreeRepository;

    @Override
    @Transactional
    public List<ChristmasTree> findAll() {
        return christmasTreeRepository.findAll();
    }

    @Override
    @Transactional
    public ChristmasTree findById(long id) {
        return christmasTreeRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public List<ChristmasTree> bulkCreate(List<ChristmasTree> christmasTrees) {
        return christmasTreeRepository.saveAll(christmasTrees);
    }
}
